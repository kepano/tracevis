# TRACEVIS - Visualize and interactively explore execution traces#

Tracevis is a tool based on Python and matplotlib to visualise execution traces containing time-interval and time-value data.

To get an overview of the command line usage type:

    $ tracevis.py  --help
    	
    usage: tracevis.py [-h] [--version] [--showDurations]  [--timeUnit TIMEUNIT]
                       [--measurePeriodic MEASUREPERIODIC]
                       [--traceDiagramYRatio TRACEDIAGRAMYRATIO]
                       [--graphDiagramYRatio GRAPHDIAGRAMYRATIO]
                       [--createSampleTrace CREATESAMPLETRACE]
                       [traceFileList [traceFileList ...]]
    
    positional arguments:
      traceFileList         list of trace files
    
    optional arguments:
      -h, --help            show this help message and exit
      --version             print program information and version
      --showDurations       show duration for each element
      --timeUnit TIMEUNIT   define unit of x axis (default is 'ms')
      --measurePeriodic MEASUREPERIODIC
                            comma-seperated list of events which peridically occur
      --traceDiagramYRatio TRACEDIAGRAMYRATIO
                            Y size ratio of the trace diagram
      --graphDiagramYRatio GRAPHDIAGRAMYRATIO
                            Y size ratio of the graph diagram
      --createSampleTrace CREATESAMPLETRACE
                            Creates a sample trace file to explain and demonstrace
                            the syntax
	$

To generate an example trace use

    $ tracevis.py  --createSampleTrace sample.tvis
    $

To visualize the trace use

	$ tracevis.py --showDurations sample.tvis
    
    Reading trace..........................................................................
    Preparing visualization

![Application snapshot](snapshot.png)

### Interactive exploring ###
Additionally to the functions in the Matplotlib navigation toolbar you can use markers. A left-click into the diagram will 
set a base marker; when moving the cursor over the diagram the time difference relative to the base marker is shown; by 
another left-click  more markers can be added, for which the time difference relative to the base marker is shown left to 
the marker line; pressing the right mouse button will remove the marker most recently added marker and at last the base marker.

![Application snapshot](snapshotWithMarker.png)

### Syntax ###

Tracevis syntax is straight forward. Every line correctponds to one data point of a dataset.
To visualize **traces** with horizontal bars (depicting the time intervals in which an action is 
ongoing, a condition is fulfilled, etc.) use lines of the following format:

     trace: event_name: start_time.._stop_time: duration

It's possible group different traces into **subgraphs**. In the following
events e1 and e2 are displayed in one subgraph (sg1) and e3, e4, and d1_e5
in another subgraph sg2:

     trace: e1_sg1: start_time..stop_time: duration
     ...
     trace: e2_sg1: start_time..stop_time: duration
     ...
     trace: e3_sg2: start_time..stop_time: duration
     ...
     trace: e4_sg2: start_time..stop_time: duration
     ...
     trace: e5_sg2: start_time..stop_time: duration
     ...

To visualize **time-value pairs** over time use

     points: event_name: time: value

or 

    points_connected: event_name: time: value

to **linearly connect** subsequent pairs or

     steps: event_name: time: value

to visualize in form of a **step function**. Similarly to ``trace``, also subgraphs can be defined.

You can define the **order in which the events are drawn** in the individual diagrams:

     event_order: e1, e2, e3, e4, e5, g1, g2, s3, s4

**Comments** as these lines start with an ``#``.
