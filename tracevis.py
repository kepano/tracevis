#  Copyright (c) 2020 Stephan Zimmer
#  All rights reserved.
#  
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#  * Redistributions of source code must retain the above copyright notice, this 
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice, this 
#    list of conditions and the following disclaimer in the documentation and/or 
#    other materials provided with the distribution.
#  * Neither the name of Stephan Zimmer nor the names of its contributors may 
#    be used to endorse or promote products derived from this software without 
#    specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
#  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
#  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
#  SHALL STEPHAN ZIMMER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
#  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
#  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

#! /opt/local/bin/python

import io
import matplotlib.pyplot as plt
import argparse
import re
import sys
import os
from functools import reduce
import operator
from functools import cmp_to_key

class InteractionManager:
    class Marker:
        def __init__(self, subplots, line_props, text_props):
            self._subplots = subplots
            self._lines = []
            self._x_pos = 0

            for subplot in self._subplots:
                self._lines.append(subplot.axvline(x=self._x_pos, **line_props))
                self._lines[-1].set_visible(False)

            self._text = self._subplots[0].text(0, 0, '', text_props)
            self._visible = False

        def set_visible(self, visible):
            self._visible = visible

            for line in self._lines:
                line.set_visible(self._visible)
            
            self._text.set_visible(self._visible)
        
        def is_visible(self):
            return self._visible

        def update_text(self, position, text):
            self._text.set_text(text)
            self._text.set_position(position)
            self.set_visible(True)

        def update_line(self, x_position):
            self._x_pos = x_position
            self._text.set_text('')
            
            for line in self._lines:
                line.set_xdata(x_position)

            self.set_visible(True)
        
        def get_x_pos(self):
            return self._x_pos

        def remove(self):
            for line in self._lines:
                line.remove()

            self._text.remove()

        def get_line_widgets(self):
            return self._lines

        def get_text_widget(self):
            return self._text

    def __init__(self, canvas, subplots, unit='ms', base_marker_line_props={}, base_marker_text_props={}, marker_line_props={}, marker_text_props={}, cursor_props={}, use_blit=True):
        self._canvas = canvas
        self._subplots = subplots
        self._unit = unit

        self._mouse_button_pressing = False
        self._mouse_moving = False

        self._use_blit = use_blit
        if self._use_blit and not self._canvas.supports_blit:
            print("Note: blitting is not supported on this platform")
            self._use_blit = False

        self._cursor_props = cursor_props
        self._cursor_props['animated'] = self._use_blit
        self._marker_line_props = marker_line_props
        self._marker_text_props = marker_text_props
        self._base_marker_line_props = base_marker_line_props
        self._base_marker_text_props = base_marker_text_props
        self._base_marker_text_props['animated'] = self._use_blit

        self._base_marker = self.Marker(subplots, self._base_marker_line_props, self._base_marker_text_props)

        self._cursor = self.Marker(subplots, self._cursor_props, {})
        self._cursor.set_visible(True)

        self._animated_artists = []
        if self._use_blit:
            self._animated_artists = self._cursor.get_line_widgets() + [self._base_marker.get_text_widget()]
        
        for artist in self._animated_artists:
            artist.set_animated(True)

        self._marker = []

        self.connect()

    def connect(self):
        self._onpress_cid = self._canvas.mpl_connect('button_press_event', self.onpress)
        self._onrelease_cid = self._canvas.mpl_connect('button_release_event', self.onrelease)
        self._onmove_cid = self._canvas.mpl_connect('motion_notify_event', self.onmove)
        self._ondraw_cid = self._canvas.mpl_connect("draw_event", self.on_draw)

    def disconnect(self):
        self._canvas.mpl_disconnect(self._onpress_cid)
        self._canvas.mpl_disconnect(self._onrelease_cid)
        self._canvas.mpl_disconnect(self._onmove_cid)
        self._canvas.mpl_disconnect(self._ondraw_cid)

    def onpress(self, event):
        if event.button == 1:
            self._mouse_button_pressing = True

    def onmove(self, event):
        if event.button == 1:
            if self._mouse_button_pressing:
                self._mouse_moving = True

        if self._base_marker.is_visible() and event and event.xdata:
            self._base_marker.update_text((self._base_marker.get_x_pos() + 0.3, self._subplots[0].get_ylim()[1]-0.6), f'{round(event.xdata - self._base_marker.get_x_pos(), 1)} {self._unit}')

        self._cursor.update_line(event.xdata)

        self.update_animated_artists()

        if not self._use_blit:
            self._canvas.draw()

    def update_animated_artists(self):
        if self._use_blit:
            self._canvas.restore_region(self._background)
            self.draw_animated_artists()
            self._canvas.blit(self._canvas.figure.bbox)
            self._canvas.flush_events()

    def onrelease(self,event):
        if event.button != 1:
            if self._marker:
                marker = self._marker.pop(-1)
                marker.remove()
            else:
                self._base_marker.set_visible(False)
            
            self._canvas.draw()

        else:
            if self._mouse_button_pressing and not self._mouse_moving:
                if event.button == 1:
                    if not self._base_marker.is_visible():
                        self._base_marker.update_line(event.xdata)
                    else:
                        marker = InteractionManager.Marker(self._subplots, self._marker_line_props, self._marker_text_props)
                        marker.update_line(event.xdata)
                        marker.update_text((event.xdata+0.3, self._subplots[0].get_ylim()[1]-0.6), f'{round(event.xdata - self._base_marker.get_x_pos(), 1)} {self._unit}')
                        
                        self._marker.append(marker)

            self._mouse_button_pressing = False
            self._mouse_moving = False

    def on_draw(self, event):
        self._background = self._canvas.copy_from_bbox(self._canvas.figure.bbox)
        self.draw_animated_artists()

    def draw_animated_artists(self):
        for artist in self._animated_artists:
            self._canvas.figure.draw_artist(artist)

class TraceVisualization:
    bar_colors = [
        '#1f77b4'
      , '#aec7e8'
      , '#ff7f0e'
      , '#ffbb78'
      , '#2ca02c'
      , '#98df8a'
      , '#d62728'
      , '#ff9896'
      , '#9467bd'
      , '#c5b0d5'
      , '#8c564b'
      , '#c49c94'
      , '#e377c2'
      , '#f7b6d2'
      , '#7f7f7f'
      , '#c7c7c7'
      , '#bcbd22'
      , '#dbdb8d'
      , '#17becf'
      , '#9edae5'
    ]
    graph_colors = [
        '#1f77b4'
      , '#ff7f0e'
      , '#2ca02c'
      , '#d62728'
      , '#9467bd'
      , '#8c564b'
      , '#e377c2'
      , '#7f7f7f'
      , '#bcbd22'
      , '#17becf'
    ]

    def __init__(self, tracefile_list, unit, measure_periodic, show_durations, traceDiagramYRatio, graphDiagramYRatio, use_blit=True):
        self._unit = unit
        self._measure_periodic = measure_periodic
        self._show_durations = show_durations

        self._marker_lines = None
        self._marker_x = None

        self._use_blit = use_blit

        self._traceDiagramYRatio = traceDiagramYRatio
        self._graphDiagramYRatio = graphDiagramYRatio

        self._tracefile_list = tracefile_list

        self.read_data(tracefile_list)
        self.create_visualization()

    def read_data(self, trace_file_list):
        self._markers = []
        self._trace_data = dict()
        self._connected_points_data = dict()
        self._points_data = dict()
        self._steps_data = dict()
        self._events_in_trace_diagram = dict()
        self._events_in_time_value_diagrams = dict()
        self._event_order = list()
        _markers = self._markers
        _trace_data = self._trace_data
        _connected_points_data = self._connected_points_data
        _points_data = self._points_data
        _steps_data = self._steps_data
        _events_in_trace_diagram = self._events_in_trace_diagram
        _events_in_time_value_diagrams = self._events_in_time_value_diagrams

        re_comments = re.compile(r'\s*#').search
        re_trace_data = re.compile(r'\s*trace:\s*(((.*)_)?(.*)):\s*(\d+(\.\d+)?)\s*\.\.\s*(\d+(\.\d+)?)\s*:\s*(\d+(\.\d+)?)').search
        re_time_value_data = re.compile(r'\s*(points|points_connected|steps):\s*(((.*)_)?(.*))\s*:\s*(\d+(\.\d+)?)\s*:\s*(-?\d+(\.\d+)?)').search
        re_event_order = re.compile(r'\s*event_order:\s*(.+)$').search

        for file_name in trace_file_list:
            for line in open(file_name, "r"):
                if re_comments(line):
                    continue

                if not self._event_order:
                    searchObj = re_event_order(line)
                    if searchObj:
                        self._event_order = [e.strip() for e in searchObj.group(1).split(",")]

                searchObj = re_trace_data(line)
                if searchObj:
                    name = searchObj.group(4)
                    diagram = searchObj.group(3) or 'defaultTrace'
                    start = float(searchObj.group(5))
                    duration = float(searchObj.group(9))

                    if diagram not in _events_in_trace_diagram:
                        _events_in_trace_diagram[diagram] = []
                    if name not in _events_in_trace_diagram[diagram]:
                        _events_in_trace_diagram[diagram].append(name)

                    if name not in _trace_data:
                        _trace_data[name] = []

                    _trace_data[name].append([start, duration])
                    continue

                searchObj = re_time_value_data(line)
                if searchObj:
                    type = searchObj.group(1)
                    name = searchObj.group(5)
                    diagram = searchObj.group(4) or 'defaultPoints'
                    value_t = float(searchObj.group(6))
                    value_y = float(searchObj.group(8))

                    if diagram not in _events_in_time_value_diagrams:
                        _events_in_time_value_diagrams[diagram] = []
                    if name not in _events_in_time_value_diagrams[diagram]:
                        _events_in_time_value_diagrams[diagram].append(name)

                    if type == 'points_connected':
                        if name not in _connected_points_data:
                            _connected_points_data[name] = []

                        _connected_points_data[name].append([value_t, value_y])
                    elif type == 'points':
                        if name not in _points_data:
                            _points_data[name] = []

                        _points_data[name].append([value_t, value_y])
                    elif type == 'steps':
                        if name not in _steps_data:
                            _steps_data[name] = []

                        _steps_data[name].append([value_t, value_y])
                    continue

                #searchObj = re.search(r'\s*(\d+(\.\d+)?)\s*:\s*(.*)', line)
                #
                #if searchObj:
                #    start = float(searchObj.group(1))
                #    text = searchObj.group(3)
                #
                #    self._markers.append([start, text])

        self._event_order += [e for e in [e for l in self._events_in_trace_diagram.values() for e in l] if e not in self._event_order]
        self._event_order += [e for e in [e for l in self._events_in_time_value_diagrams.values() for e in l] if e not in self._event_order]

        for v in self._events_in_trace_diagram.values():
            v.sort(key=cmp_to_key(lambda x, y: self._event_order.index(x) - self._event_order.index(y)), reverse=True)

        for v in self._events_in_time_value_diagrams.values():
            v.sort(key=cmp_to_key(lambda x, y: self._event_order.index(x) - self._event_order.index(y)), reverse=True)

        common_ids = set.union(
            set(self._trace_data.keys()).intersection(set(self._connected_points_data.keys())),
            set(self._trace_data.keys()).intersection(set(self._points_data.keys())),
            set(self._trace_data.keys()).intersection(set(self._steps_data.keys())),
            set(self._connected_points_data.keys()).intersection(set(self._points_data.keys())),
            set(self._connected_points_data.keys()).intersection(set(self._steps_data.keys())),
            set(self._points_data.keys()).intersection(set(self._steps_data.keys()))
            )

        if len(common_ids) > 0:
            print(f'\nError: IDs must be unique. The following IDs are used for several categories: {common_ids}\n', file=sys.stderr)
            exit(1)

        for e in self._trace_data:
            self._trace_data[e] = sorted(self._trace_data[e], key=lambda t: t[0])

        for g in self._connected_points_data:
            self._connected_points_data[g] = sorted(self._connected_points_data[g], key=lambda t: t[0])

        for p in self._points_data:
            self._points_data[p] = sorted(self._points_data[p], key=lambda t: t[0])

        for p in self._steps_data:
            self._steps_data[p] = sorted(self._steps_data[p], key=lambda t: t[0])

    def create_visualization(self):
        annotations = dict()
        for pe in self._measure_periodic:
            if pe not in annotations:
                annotations[pe] = []

            x0 = None
            for x1 in self._trace_data[pe]:
                if not x0:
                    x0 = x1
                    continue

                annotations[pe].append([f'{round(x1[0]-x0[0], 1)}', (x0[0]+x0[1])+((x1[0]-(x0[0]+x0[1]))/2)])
                x0 = x1

        print("\nPreparing visualization", end='')
        sys.stdout.flush()

        num_subplots = len(self._events_in_trace_diagram) + len(self._events_in_time_value_diagrams)

        gridspecs = {}
        if num_subplots > 1:
            total = 0

            for data_set_name in self._events_in_trace_diagram.keys():
                total = total + len(self._events_in_trace_diagram[data_set_name])
            total = float(total)

            ratios = []
            for data_set_name in self._events_in_trace_diagram.keys():
                ratios.append(len(self._events_in_trace_diagram[data_set_name])/total * self._traceDiagramYRatio)
            for data_set_name in self._events_in_time_value_diagrams.keys():
                ratios.append(self._graphDiagramYRatio)

            gridspecs = {'height_ratios': ratios}

        self._fig, self._subplots = plt.subplots(num_subplots, 1, figsize=(8.4,4.8), sharex=True, gridspec_kw=gridspecs)

        self._fig.canvas.set_window_title(f'Tracevis - ' + reduce(operator.add, self._tracefile_list, ""))

        if num_subplots == 1:
            self._subplots = [self._subplots]

        color_index = 0
        for data_set_index, data_set_name in enumerate(self._events_in_trace_diagram.keys()):
            axis = self._subplots[data_set_index]
            axis.grid(True, axis='y', linestyle='dotted')
            i=0
            labels = []

            for name in self._events_in_trace_diagram[data_set_name]:
                time_stamps = self._trace_data[name]

                labels.append(name)

                axis.broken_barh(time_stamps, (i-0.4,0.8), color=self.bar_colors[color_index % len(self.bar_colors)] )
                axis_text = axis.text
                if self._show_durations:
                    for xs, xd in time_stamps:
                        axis_text(x=xs + xd/2, 
                            y=i,
                            s=f'{round(xd, 1)} {self._unit}', 
                            ha='center', 
                            va='center',
                            color='white',
                            )
                if name in annotations:
                    for a in annotations[name]:
                        axis_text(x=a[1], 
                            y=i,
                            s=a[0], 
                            ha='center', 
                            va='center',
                            color='black',
                        )
                i = i + 1
                color_index = color_index + 1
            
            axis.set_yticks(range(len(labels)))
            axis.set_yticklabels(labels) 

        # for mtime, mtext in self._markers:
        #     plt.axvline(x=mtime, color="gray", linestyle="--")
        # 
        #     self._ax.text(x=mtime, 
        #         y=0.5,
        #         s=mtext, 
        #         ha='center', 
        #         va='center',
        #         color='gray',
        #         weight='bold'
        #     )

        color_index = 0
        for data_set_index, data_set_name in enumerate(self._events_in_time_value_diagrams.keys()):
            axis = self._subplots[len(self._events_in_trace_diagram) + data_set_index]
            axis.grid(True, axis='y', linestyle='dotted')
            for data_set_name in self._events_in_time_value_diagrams[data_set_name]:
                if data_set_name in self._connected_points_data.keys():
                    axis.plot([t for [t, _] in self._connected_points_data[data_set_name]], [y for [_, y] in self._connected_points_data[data_set_name]], label=data_set_name, linestyle='-', color=self.graph_colors[color_index % len(self.graph_colors)])
                    color_index = color_index + 1

                if data_set_name in self._points_data.keys():                    
                    axis.plot([t for [t, _] in self._points_data[data_set_name]], [y for [_, y] in self._points_data[data_set_name]], label=data_set_name, linestyle='', marker='o', color=self.graph_colors[color_index % len(self.graph_colors)])
                    color_index = color_index + 1

                if data_set_name in self._steps_data.keys():                    
                    axis.step([t for [t, _] in self._steps_data[data_set_name]], [y for [_, y] in self._steps_data[data_set_name]], label=data_set_name, color=self.graph_colors[color_index % len(self.graph_colors)], where='post')
                    color_index = color_index + 1

            axis.legend(loc="best")

        self._subplots[-1].set_xlabel(self._unit)
        plt.tight_layout()
        print()

        # self._annot = self._ax.annotate("", xy=(100,100), xytext=(120,120),
        #                   textcoords="offset points",
        #                   bbox=dict(boxstyle="round", fc="w"),
        #                   arrowprops=dict(arrowstyle="->"))
        # self._annot.set_text("fevrt")
        # self._annot.get_bbox_patch().set_facecolor('blue')
        # self._annot.get_bbox_patch().set_alpha(0.4)
        # self._annot.set_visible(True)

        self._interaction_manager = InteractionManager(self._fig.canvas, self._subplots, unit=self._unit,
            cursor_props = { 'color': 'r', 'lw': 1},
            marker_line_props = {'color': 'darkblue', 'lw': 1},
            marker_text_props = {'color': 'darkblue'},
            base_marker_line_props = {'color': 'r', 'lw': 1, 'linestyle': '-'},
            base_marker_text_props = {'color': 'darkred'},
            use_blit=self._use_blit)

    def show_modal(self):
        plt.show()

def main():
    cmdArgParser = argparse.ArgumentParser()
    cmdArgParser.add_argument("--version", help="print program information and version", action="store_true")
    cmdArgParser.add_argument("--license", help="provide information on licensing and external contributions", action="store_true")
    cmdArgParser.add_argument("--showDurations", help="show duration for each element", action="store_true")
    cmdArgParser.add_argument("--timeUnit", help="define unit of x axis (default is 'ms')", action="store")
    cmdArgParser.add_argument("--measurePeriodic", help="comma-seperated list of events which peridically occur", action="store")
    cmdArgParser.add_argument("--traceDiagramYRatio", help="Y size ratio of the trace diagram", action='store')
    cmdArgParser.add_argument("--graphDiagramYRatio", help="Y size ratio of the graph diagram", action='store')
    cmdArgParser.add_argument("--createSampleTrace", help="Creates a sample trace file to explain and demonstrace the syntax", action='store')
    cmdArgParser.add_argument("--useBlit", help=argparse.SUPPRESS, action='store')

    cmdArgParser.add_argument("traceFileList", nargs='*', help="list of trace files")
    cmdArgs = cmdArgParser.parse_args()

    if cmdArgs.version:
        print('''tracevis.py version 0.6
Copyright (c) 2020 Stephan Zimmer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
EXPRESS OR IMPLIED WARRANTIES. For details on licensing matters and external contributions invoke with --license.
''')
        exit(0)

    if cmdArgs.license:
        print('''tracevis.py - Copyright (c) 2020 Stephan Zimmer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this 
  list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this 
  list of conditions and the following disclaimer in the documentation and/or 
  other materials provided with the distribution.
* Neither the name of Stephan Zimmer nor the names of its contributors may 
  be used to endorse or promote products derived from this software without 
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
SHALL STEPHAN ZIMMER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
''')
        exit(0)


    unit='ms'
    if cmdArgs.timeUnit:
        unit = cmdArgs.timeUnit

    if cmdArgs.createSampleTrace:
        if os.path.exists(cmdArgs.createSampleTrace):
            print(f'The file {cmdArgs.createSampleTrace} already exists. Please choose a different filename.', file=sys.stderr)
            exit(1)

        with open(cmdArgs.createSampleTrace, "a") as f:
            f.write('''# Tracevis syntax
#
# Tracevis syntax is straight forward. Every line correctponds to one data point of a dataset.
#
# To visualize TRACES with horizontal bars (depicting the time intervals in which an action is 
# ongoing, a condition is fulfilled, etc.) use lines of the following format:
#      trace: event_name: start_time.._stop_time: duration
#
# It's possible group different traces into SUBGRAPHS. In the following
# events e1 and e2 are displayed in one subgraph (sg1) and e3, e4, and d1_e5
# in another subgraph sg2:
#      trace: e1_sg1: start_time..stop_time: duration
#      ...
#      trace: e2_sg1: start_time..stop_time: duration
#      ...
#      trace: e3_sg2: start_time..stop_time: duration
#      ...
#      trace: e4_sg2: start_time..stop_time: duration
#      ...
#      trace: e5_sg2: start_time..stop_time: duration
#      ...
#
# To visualize (TIME, VALUE) PAIRS over time use
#      points: event_name: time: value
#
# or 
#      points_connected: event_name: time: value
#
# to LINEARLY CONNECT subsequent pairs
#
# or 
#      steps: event_name: time: value
#
# to visualize in form of a STEP FUNCTION. Similarly to "trace", also subgraphs can be defined.
#
# You can define the order in which the events will be visualized as follows:
#      event_order: e1, e2, e3, e4, e5, g1, g2, s3, s4
#
# COMMENTS as these lines start with an '#'. A CONCREATE EXAMPLE follows below.
#

event_order: e1, e2, e3

trace: e1: 4.5..6: 1.5
trace: d1_e4: 1..2: 1
trace: d1_e3: 1.5..3: 1.5
trace: e2: 4.5..7: 2.5
trace: d1_e5: 7..9: 2
trace: e2: 7.5..12.0: 4.5
trace: d1_e4: 8..9: 1
trace: e1: 8..10.5: 1.5
trace: d1_e3: 8..10.5: 1.5
trace: d1_e5: 11..13: 2
trace: d1_e4: 11..20.1: 9.1
trace: e1: 12.5..18.5: 6
trace: d1_e5: 15..17: 2
trace: e2: 16..26.5: 10.5
trace: d1_e5: 19..21: 2
trace: d1_e3: 20..20.3: 0.3
trace: e1: 22..23.5: 1.5
trace: d1_e5: 23..25: 2
trace: d1_e5: 27..29: 2
points_connected: g1: 3: 5
points_connected: g1: 4: 5.5
points_connected: g1: 5: 6
points_connected: g1: 6: 6.5
points_connected: g1: 7: 7
points_connected: g1: 8: 7.5
points_connected: g1: 9: 8
points_connected: g1: 10: 8
points_connected: g1: 11: 8
points_connected: g1: 12: 8
points_connected: g1: 13: 4
points_connected: g1: 14: 0
points_connected: g1: 15: -4
points_connected: g1: 16: -8
points_connected: g1: 17: -8
points_connected: g1: 18: -8
points_connected: g1: 19: -8
points_connected: g1: 20: -8
points_connected: g1: 21: -8
points_connected: g1: 22: -8
points_connected: g1: 23: -8
points_connected: g1: 24: -8
points_connected: g1: 25: -8
points_connected: g1: 26: -8
points_connected: d3_g2: 3: 105
points_connected: d3_g2: 4: 105.5
points_connected: d3_g2: 5: 106
points_connected: d3_g2: 6: 106.5
points_connected: d3_g2: 7: 107
points_connected: d3_g2: 8: 107.5
points_connected: d3_g2: 9: 108
points_connected: d3_g2: 10: 108
points_connected: d3_g2: 11: 108
points_connected: d3_g2: 12: 108
points_connected: d3_g2: 13: 104
points_connected: d3_g2: 14: 100
points_connected: d3_g2: 15: -104
points_connected: d3_g2: 16: -108
points_connected: d3_g2: 17: -108
points_connected: d3_g2: 18: -108
points_connected: d3_g2: 19: -108
points_connected: d3_g2: 20: -108
points_connected: d3_g2: 21: -108
points_connected: d3_g2: 22: -108
points_connected: d3_g2: 23: -108
points_connected: d3_g2: 24: -108
points_connected: d3_g2: 25: -108
points_connected: d3_g2: 26: -108
steps: d4_s3: 4: 1
steps: d4_s3: 10: 0
steps: d4_s3: 15: 1
steps: d4_s3: 26: 0
steps: d4_s4: 2: 0
steps: d4_s4: 13: 1
steps: d4_s4: 20: 0
''')
        exit(0)

    measure_periodic = []
    if cmdArgs.measurePeriodic:
        measure_periodic = cmdArgs.measurePeriodic.split(",")

    use_blit = True
    if cmdArgs.useBlit:
        use_blit = cmdArgs.useBlit

    trace_file_list = []
    if len(cmdArgs.traceFileList) == 0:
        print("Please specify list of traces", file=sys.stderr)
        exit(1)
    else:
        trace_file_list = cmdArgs.traceFileList

    traceDiagramYRatio = 2.5
    graphDiagramYRatio = 1
    
    if cmdArgs.traceDiagramYRatio or cmdArgs.graphDiagramYRatio:
        traceDiagramYRatio = 1
        graphDiagramYRatio = 1

    if cmdArgs.traceDiagramYRatio:
        traceDiagramYRatio = float(cmdArgs.traceDiagramYRatio)

    if cmdArgs.graphDiagramYRatio:
        graphDiagramYRatio = float(cmdArgs.graphDiagramYRatio)

    print("Reading trace", end='')
    sys.stdout.flush()

    trace_visualization = TraceVisualization(trace_file_list, unit, measure_periodic, cmdArgs.showDurations, traceDiagramYRatio, graphDiagramYRatio, use_blit=use_blit)

    trace_visualization.show_modal()

if __name__ == "__main__":
    main()
